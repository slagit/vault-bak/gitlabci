package runner

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetProject(t *testing.T) {
	json := `{"id": 1, "path_with_namespace": "one", "runners_token": "test-reg-token"}`
	r := ioutil.NopCloser(bytes.NewReader([]byte(json)))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		assert.EqualValues(t, "GET", req.Method)
		assert.EqualValues(t, "/api/v4/projects/my%2Ftest%2Fpath", req.URL.EscapedPath())
		assert.EqualValues(t, "Bearer test-access-token", req.Header.Get("Authorization"))
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       r,
		}, nil
	})
	ctx := context.Background()

	project, err := client.GetProject(
		ctx,
		&GetProjectOptions{
			URL:               "http://gitlab.example.com",
			AccessToken:       "test-access-token",
			PathWithNamespace: "my/test/path",
		},
	)
	assert.Nil(t, err)
	assert.EqualValues(t, 1, project.ID)
	assert.EqualValues(t, "one", project.PathWithNamespace)
	assert.EqualValues(t, "test-reg-token", project.RunnersToken)
}

func TestEnableRunner(t *testing.T) {
	json := `{"id": 1, "path_with_namespace": "one", "runners_token": "test-reg-token"}`
	r := ioutil.NopCloser(bytes.NewReader([]byte(json)))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		assert.EqualValues(t, "POST", req.Method)
		assert.EqualValues(t, "/api/v4/projects/my%2Ftest%2Fpath/runners", req.URL.EscapedPath())
		assert.EqualValues(t, "Bearer test-access-token", req.Header.Get("Authorization"))
		return &http.Response{
			StatusCode: http.StatusCreated,
			Body:       r,
		}, nil
	})
	ctx := context.Background()

	err := client.EnableRunner(
		ctx,
		&EnableRunnerOptions{
			URL:               "http://gitlab.example.com",
			AccessToken:       "test-access-token",
			PathWithNamespace: "my/test/path",
			RunnerID:          1,
		},
	)
	assert.Nil(t, err)
}

func TestGetRunnerByDescription(t *testing.T) {
	json := `[{"id":1,"description":"one"},{"id":2,"description":"two"},{"id":3,"description":"three"}]`
	r := ioutil.NopCloser(bytes.NewReader([]byte(json)))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		assert.EqualValues(t, "GET", req.Method)
		assert.EqualValues(t, "Bearer test-access-token", req.Header.Get("Authorization"))
		assert.EqualValues(t, "/api/v4/runners", req.URL.Path)
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       r,
		}, nil
	})
	ctx := context.Background()

	runner, err := client.GetRunnerByDescription(
		ctx,
		&GetRunnerByDescriptionOptions{
			URL:         "http://gitlab.example.com",
			AccessToken: "test-access-token",
			Description: "two",
		},
	)
	assert.Nil(t, err)
	assert.NotNil(t, runner)
	assert.EqualValues(t, 2, runner.ID)
}

func TestGetRunnerByDescriptionPage(t *testing.T) {
	json1 := `[{"id":1,"description":"one"},{"id":2,"description":"two"},{"id":3,"description":"three"}]`
	r1 := ioutil.NopCloser(bytes.NewReader([]byte(json1)))
	json2 := `[{"id":4,"description":"four"},{"id":5,"description":"five"},{"id":6,"description":"six"}]`
	r2 := ioutil.NopCloser(bytes.NewReader([]byte(json2)))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		assert.EqualValues(t, "GET", req.Method)
		assert.EqualValues(t, "Bearer test-access-token", req.Header.Get("Authorization"))
		assert.EqualValues(t, "/api/v4/runners", req.URL.Path)
		if req.URL.Query().Get("page") == "" {
			return &http.Response{
				StatusCode: http.StatusOK,
				Body:       r1,
				Header: http.Header{
					"X-Next-Page": {"2"},
				},
			}, nil
		}
		assert.EqualValues(t, "2", req.URL.Query().Get("page"))
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       r2,
		}, nil
	})
	ctx := context.Background()

	runner, err := client.GetRunnerByDescription(
		ctx,
		&GetRunnerByDescriptionOptions{
			URL:         "http://gitlab.example.com",
			AccessToken: "test-access-token",
			Description: "five",
		},
	)
	assert.Nil(t, err)
	assert.NotNil(t, runner)
	assert.EqualValues(t, 5, runner.ID)
}

func TestGetRunnerByDescriptionMissing(t *testing.T) {
	json := `[]`
	r := ioutil.NopCloser(bytes.NewReader([]byte(json)))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		assert.EqualValues(t, "GET", req.Method)
		assert.EqualValues(t, "Bearer test-access-token", req.Header.Get("Authorization"))
		assert.EqualValues(t, "/api/v4/runners", req.URL.Path)
		return &http.Response{
			StatusCode: http.StatusOK,
			Body:       r,
		}, nil
	})
	ctx := context.Background()

	runner, err := client.GetRunnerByDescription(
		ctx,
		&GetRunnerByDescriptionOptions{
			URL:         "http://gitlab.example.com",
			AccessToken: "test-access-token",
			Description: "two",
		},
	)
	assert.NotNil(t, err)
	assert.Nil(t, runner)
}

func TestRemoveRunner(t *testing.T) {
	r := ioutil.NopCloser(bytes.NewReader([]byte{}))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		assert.EqualValues(t, "DELETE", req.Method)
		assert.EqualValues(t, "gitlab.example.com", req.URL.Host)
		assert.EqualValues(t, "/api/v4/runners/5", req.URL.Path)
		return &http.Response{
			StatusCode: http.StatusNoContent,
			Body:       r,
		}, nil
	})
	ctx := context.Background()

	err := client.RemoveRunner(
		ctx,
		&RemoveRunnerOptions{
			URL:         "http://gitlab.example.com",
			AccessToken: "test-access-token",
			RunnerID:    5,
		},
	)
	assert.Nil(t, err)
}
