package gitlabci

import (
	"context"
	"fmt"
	"time"

	"github.com/hashicorp/errwrap"
	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/helper/base62"
	"github.com/hashicorp/vault/sdk/logical"
	"gitlab.com/slagit/vault/gitlabci/runner"
)

func tokens(b *backend) *framework.Secret {
	return &framework.Secret{
		Type: "token",
		Fields: map[string]*framework.FieldSchema{
			"authentication_token": {
				Type:        framework.TypeString,
				Description: "Authentication token used to authenticate an instance of a GitLab runner",
			},
			"gitlab_url": {
				Type:        framework.TypeString,
				Description: "URL used to access the GitLab instance",
			},
		},
		Renew:  b.tokensRenew,
		Revoke: b.tokensRevoke,
	}
}

func swapWalID(ctx context.Context, s logical.Storage, walID, kind string, data interface{}) (string, error) {
	newWalID, err := framework.PutWAL(ctx, s, kind, data)
	if err != nil {
		return walID, err
	}

	err = framework.DeleteWAL(ctx, s, walID)
	if err != nil {
		return walID, errwrap.Wrapf("failed to commit WAL entry: {{err}}", err)
	}

	return newWalID, nil
}

func (b *backend) tokenCreate(ctx context.Context, s logical.Storage, displayName, roleName string, role *gitlabciRoleEntry) (*logical.Response, error) {
	uuid, err := base62.Random(20)
	if err != nil {
		return nil, err
	}

	timestamp := fmt.Sprint(time.Now().Unix())

	description := fmt.Sprintf("v-%s-%s-%s", roleName, uuid, timestamp)

	client := b.client()
	project, err := client.GetProject(ctx, &runner.GetProjectOptions{
		AccessToken:       role.AccessToken,
		PathWithNamespace: role.ProjectPath[0],
		URL:               role.GitlabUrl,
	})
	if err != nil {
		return logical.ErrorResponse(fmt.Sprintf("Error looking up project: %s", err)), err
	}

	walID, err := framework.PutWAL(ctx, s, "description", walDescription{
		AccessToken: role.AccessToken,
		Description: description,
		GitlabURL:   role.GitlabUrl,
	})
	if err != nil {
		return nil, err
	}

	id, token, err := client.RegisterRunner(ctx, &runner.RegisterRunnerOptions{
		URL:               role.GitlabUrl,
		RegistrationToken: project.RunnersToken,
		Description:       description,
		Tags:              role.Tags,
		Locked:            role.Locked,
	})
	if err != nil {
		return logical.ErrorResponse(fmt.Sprintf("Error registering runner: %s", err)), err
	}

	if len(role.ProjectPath) > 1 {
		walID, err = swapWalID(ctx, s, walID, "auth_token", walAuthnToken{
			AuthnToken: token,
			GitlabURL:  role.GitlabUrl,
		})
		if err != nil {
			return nil, err
		}

		for _, projectPath := range role.ProjectPath[1:] {
			err = client.EnableRunner(ctx, &runner.EnableRunnerOptions{
				AccessToken:       role.AccessToken,
				PathWithNamespace: projectPath,
				RunnerID:          id,
				URL:               role.GitlabUrl,
			})
			if err != nil {
				return logical.ErrorResponse(fmt.Sprintf("Error enabling runner: %s", err)), err
			}
		}
	}

	err = framework.DeleteWAL(ctx, s, walID)
	if err != nil {
		return nil, errwrap.Wrapf("failed to commit WAL entry: {{err}}", err)
	}
	resp := b.Secret("token").Response(map[string]interface{}{
		"gitlab_url":           role.GitlabUrl,
		"authentication_token": token,
	}, map[string]interface{}{
		"role":                 roleName,
		"authentication_token": token,
	})
	resp.Secret.TTL = role.DefaultTTL
	resp.Secret.MaxTTL = role.MaxTTL
	return resp, nil
}

func (b *backend) tokensRenew(ctx context.Context, req *logical.Request, d *framework.FieldData) (*logical.Response, error) {
	roleNameRaw, ok := req.Secret.InternalData["role"]
	if !ok {
		return nil, fmt.Errorf("secret is missing role internal data")
	}

	role, err := b.roleRead(ctx, req.Storage, roleNameRaw.(string), true)
	if err != nil {
		return nil, err
	}
	if role == nil {
		return nil, fmt.Errorf("error during renew: could not find role with name %s", roleNameRaw.(string))
	}

	resp := &logical.Response{Secret: req.Secret}
	resp.Secret.TTL = role.DefaultTTL
	resp.Secret.MaxTTL = role.MaxTTL
	return resp, nil
}

func (b *backend) tokensRevoke(ctx context.Context, req *logical.Request, d *framework.FieldData) (*logical.Response, error) {
	roleNameRaw, ok := req.Secret.InternalData["role"]
	if !ok {
		return nil, fmt.Errorf("secret is missing role internal data")
	}

	role, err := b.roleRead(ctx, req.Storage, roleNameRaw.(string), true)
	if err != nil {
		return nil, err
	}
	if role == nil {
		return nil, fmt.Errorf("error during revoke: could not find role with name %s", roleNameRaw.(string))
	}

	client := b.client()
	err = client.UnregisterRunner(
		ctx,
		&runner.UnregisterRunnerOptions{
			URL:                 role.GitlabUrl,
			AuthenticationToken: req.Secret.InternalData["authentication_token"].(string),
		},
	)
	return nil, err
}
